# Pico Docker container for Java 8

## Recipe

(Inspired from http://sirile.github.io/2015/01/01/java-8-minicontainer-with-busybox.html "sans" the Java Runtime)

* Build a Busybox GCC GLIBC + OPKG image using Openwrt binaries (c.f. Dockerfile)
* Docker tag the thing

```shell
docker tag <your_image_id> gitlab-registry.cern.ch/busybox-pico-java8
```

* You can mount a local JRE to the pico container by using volumes

```shell
docker run -ti --rm -v /usr/lib/jvm/jre-1.8.0:/opt/jre-1.8 gitlab-registry.cern.ch/busybox-pico-java8
```

* If the volume is mapped correctly, this should output the Java help instructions.


## Important notes
* ARM variant example : https://github.com/antvale/busybox-glibc-rpi

* opkg usage is just a hack, it is not an up to date package manager. Alpine Linux seems to be the way forward. Example of GLIBC + JDK at : https://hub.docker.com/r/anapsix/alpine-java/