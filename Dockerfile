FROM scratch

ADD ./rootfs.tar /
ADD ./opkg.conf         /etc/opkg.conf
ADD ./opkg-install      /usr/bin/opkg-install
ADD ./functions.sh      /lib/functions.sh
RUN opkg-cl install http://downloads.openwrt.org/chaos_calmer/15.05/x86/64/packages/base/libgcc_4.8-linaro-1_x86_64.ipk
RUN opkg-cl install http://downloads.openwrt.org/chaos_calmer/15.05/x86/64/packages/base/libc_0.9.33.2-1_x86_64.ipk

RUN mkdir -p /opt/jre-1.8/bin
RUN echo "placeholder" > /opt/jre-1.8/bin/java
RUN chmod 755 /opt/jre-1.8/bin/java

# Link PTHREAD and Java into use, wget-ssl updates libpthread which causes Java to break
RUN ln -sf /lib/libpthread-2.18.so /lib/libpthread.so.0 

RUN ln -s /opt/jre-1.8/bin/java /usr/bin/java

CMD ["/usr/bin/java"]
